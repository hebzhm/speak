// pages/record/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    recordTextContent : '',
    btnText : '长按录音'
  },

  touchStart : function(event){
    var that = this;
    this.setData({
      btnText : '录音中。。。。。'
    });
    const recorderManager = wx.getRecorderManager()

    recorderManager.onStart(() => {
      console.log('recorder start')
    })
    recorderManager.onPause(() => {
      console.log('recorder pause')
    })
    recorderManager.onStop((res) => {
      this.setData({
        btnText: '识别中...'
      });
      console.log('recorder stop', res)
      const { tempFilePath } = res
      wx.uploadFile({
        url: "http://192.168.0.11/index.php?g=api&m=xcx&a=plupload",
        filePath: tempFilePath,

        name: "file",
        formData: {
          filetype: "audio",
          app: "speakVoice"
        },
        success: function (result) {
          
          var resultData = JSON.parse(result.data);
          console.log(resultData)
          var m4aPath = resultData.filepath;
          wx.request({
            url: 'http://192.168.0.11/index.php?g=api&m=xcx&a=getSpeechContent', //仅为示例，并非真实的接口地址
            data: {
              voice_path: m4aPath
            },
            
            header: {
              'content-type': 'application/json' // 默认值
            },
            success(res) {
              console.log(res.data)
              if (res.data.err_no==0){
                console.log(res.data.result[0])
                that.setData({
                  recordTextContent: that.data.recordTextContent + res.data.result[0],
                  btnText: '长按继续录音'
                });
              }else{
                that.setData({
                  recordTextContent: that.data.recordTextContent+'[没有听清楚]',
                  btnText: '长按继续录音'
                });
              }
            }

          });

        }
      })
    })
    recorderManager.onFrameRecorded((res) => {
      const { frameBuffer } = res
      console.log('frameBuffer.byteLength', frameBuffer.byteLength)
    })

    const options = {
      duration: 10000,
      sampleRate: 16000,
      numberOfChannels: 1,
      format: 'mp3',
    }

    recorderManager.start(options)


  },
  touchEnd : function(event){
    const recorderManager = wx.getRecorderManager()
    console.log(event)
    recorderManager.stop()
  }
})