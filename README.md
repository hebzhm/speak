# 小程序+百度语音识别
小程序语音识别录音、转格式、利用百度的语音识别接口将语音识别成文字并返回
用到的接口连接

https://developers.weixin.qq.com/miniprogram/dev/api/media/recorder/RecorderManager.html

https://ai.baidu.com/docs#/ASR-Online-PHP-SDK/top


三步走
1.服务器配置 安装ffmpeg对音频文件进行格式化

      安装ffmpeg时需要提前安装yasm插件。下面开始安装。
      wget http://www.tortall.net/projects/yasm/releases/yasm-1.3.0.tar.gz
      tar -xvf yasm-1.3.0.tar.gz
       cd yasm-1.3.0/
      ./configure && make && make install

      好了开始安装ffmpeg。
      wget http://www.ffmpeg.org/releases/ffmpeg-3.4.tar.gz
       tar -xvf ffmpeg-3.4.tar.gz
       cd ffmpeg-3.4/
       ./configure && make && make install
	   
2.后端上传文件和转码语音识别 xcxController.class.php

      上传 plupload
      转码 和语音识别 getSpeechContent
	  
3.小程序段录音并且上传
      pages/record/index.wxml
      pages/record/index.js
      
  
